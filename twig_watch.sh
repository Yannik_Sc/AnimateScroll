#!/usr/bin/env bash

#
# This script requires
# - inotify-tools
# - twig-cli (php edition from xrash: https://github.com/xrash/twig-cli)
#


TPL_PATH=demo/template
OUT_PATH=demo
CURRENT=$(pwd);

function compile {
    cd ${TPL_PATH};

    printf "Compiling ... "
    twig index.twig > ../index.html && echo "Done"

    cd ${CURRENT}
}

compile;

inotifywait -r -m -e modify ${TPL_PATH} |
    while read path action file; do
        compile;
    done;


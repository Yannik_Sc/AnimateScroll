/**
 * <div data-scroll-property="width"
 *      data-scroll-property-from="0"
 *      data-scroll-property-to="100%"
 *      data-scroll-from="0"
 *      data-scroll-to="500"></div>
 *
 * #### OR
 *
 * <div data-scroll-animation="myAnimation"
 *      data-scroll-from="0"
 *      data-scroll-to="500"></div>
 *
 * @class AnimateScroll
 * @property {jQuery} $style
 * @property {array} elements
 * @property {array} animations
 */
class AnimateProperty
{
    constructor()
    {
        this.$style = $('<style />');
        this.elements = [];
        this.animations = [];
        $(document.body).append(this.$style);

        this._registerEvents();
    }

    /**
     * @param {jQuery} $el
     */
    registerElement($el)
    {
        let animationProperty = $el.attr('data-scroll-property');
        let animationFrom = $el.attr('data-scroll-property-from');
        let animationTo = $el.attr('data-scroll-property-to');
        let scrollFrom = $el.attr('data-scroll-from');
        let scrollTo = $el.attr('data-scroll-to');
        let animation = $el.attr('data-scroll-animation');

        if (!(animationProperty === undefined || animationFrom === undefined || animationTo === undefined)) {
            animation = this._addAnimation(animationProperty, animationFrom, animationTo);
        }

        if (animation === undefined) {
            throw "no animation provided!";
        }

        this.elements.push(
            {
                $element: $el,
                from: scrollFrom,
                to: scrollTo,
                animation: animation
            }
        );

        $el.css(
            {
                'animation-name': animation,
                'animation-duration': '1s',
                'animation-play-state': 'paused',
                'animation-delay': '0s',
            }
        );

        $el.css(animationProperty, animationTo);
    }

    _addAnimation(name, from, to)
    {
        let style = this.$style.html();
        let animationName = 'as-{0}-from-{1}-to-{2}'.format(
            name,
            from.replace(/%/g, '').replace(/#/g, '').replace(/ /g, '-'),
            to.replace(/%/g, '').replace(/#/g, '').replace(/ /g, '-')
        );

        if (this.animations.indexOf(animationName) >= 0) {
            return animationName;
        }

        style += `
@keyframes {0} {
    0% {{1}: {2}}
    100% {{1}: {3}}
}`.format(
            animationName,
            name,
            from,
            to
        );

        this.$style.html(style);
        this.animations.push(animationName);
        return animationName;
    }

    _registerEvents()
    {
        $(window).on('scroll', this._onWindowScroll.bind(this));
    }

    _onWindowScroll()
    {
        let scroll = $(window).scrollTop();

        for (let i = 0; i < this.elements.length; i++) {
            let element = this.elements[i];
            this._scrollElement(element.$element, element.from, element.to, scroll);
        }
    }

    /**
     * @param {jQuery} $el
     * @param {number|string} from
     * @param {number|string} to
     * @param {number|string} scroll
     * @private
     */
    _scrollElement($el, from, to, scroll)
    {
        from = this._processUnit(from + "");
        to = this._processUnit(to + "");

        if (from > scroll) {
            $el.css('animation-delay', '0s');

            return;
        }

        if (to < scroll) {
            $el.css('animation-delay', '-1s');

            return;
        }

        let scrollStart = scroll - from;
        let scrollEnd = to - from;
        let time = Math.floor((scrollStart / scrollEnd) * 100) / 100;

        if (time > 1) {
            time = 1;
        }

        $el.css('animation-delay', -time + 's');
    }

    /**
     * @param {string} unit
     * @returns {number}
     * @private
     */
    _processUnit(unit)
    {
        if (unit.endsWith('vw')) return this._setUnitVw(unit);
        if (unit.endsWith('vh')) return this._setUnitVh(unit);
        if (unit.endsWith('%')) return this._setUnitPercentage(unit);

        return parseInt(unit);
    }

    /**
     * @param {string} unit
     * @returns {int}
     * @private
     */
    _setUnitVw(unit)
    {
        return ($(window).width() / 100) * parseInt(unit);
    }

    /**
     * @param {string} unit
     * @returns {int}
     * @private
     */
    _setUnitVh(unit)
    {
        return ($(window).height() / 100) * parseInt(unit);
    }

    /**
     * @param {string} unit
     * @returns {int}
     * @private
     */
    _setUnitPercentage(unit)
    {
        return ($(document).height() / 100) * parseInt(unit);
    }
}

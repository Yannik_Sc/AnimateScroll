/**
 * <a href="#myTarget" data-scroll-duration="time in ms"></a>
 *
 * @property {jQuery} duration
 * @property {jQuery} $target
 * @property {jQuery} $trigger
 * @property {jQuery} $html
 */
class ScrollToElement
{
    constructor($trigger)
    {
        this.duration = parseInt($trigger.attr('data-scroll-duration'));
        this.$target = $($trigger.attr('href'));
        this.$trigger = $trigger;
        this.$html = $('html');

        this._registerEvents();
    }

    _registerEvents()
    {
        this.$trigger.click(this._onClick.bind(this));
    }

    /**
     * @param {jQuery.Event} event
     * @private
     */
    _onClick(event)
    {
        event.preventDefault();

        window.history.pushState('', '', '#' + this.$target.attr('id'));
        this.$html.stop().animate({scrollTop: this.$target.offset().top}, this.duration);
    }
}

if (!String.prototype.startsWith) {
    /**
     * @param {string} expected
     * @returns {boolean}
     */
    String.prototype.startsWith = function (expected) {
        return this[0] === expected;
    }
}
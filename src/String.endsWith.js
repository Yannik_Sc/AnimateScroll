if (!String.prototype.endsWith) {
    /**
     * @param {string} search
     * @returns {boolean}
     */
    String.prototype.endsWith = function (search) {
        return this.indexOf(search) === this.length - search.length;
    };
}
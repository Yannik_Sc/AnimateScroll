String.prototype.format = function () {
    let formatted = this;

    for (let i = 0; i < arguments.length; i++) {
        formatted = formatted.replace(new RegExp('\\{' + i + '\\}', 'g'), arguments[i]);
    }

    return formatted;
};

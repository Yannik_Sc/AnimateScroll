/**
 * @property {AnimateProperty} property
 */
class AnimateScroll
{
    constructor()
    {
        this.property = new AnimateProperty();
        let self = this;

        jQuery.prototype.AnimateScroll = function () {
            for (let i = 0; i < this.length; i++) {
                self.addElement($(this[i]))
            }
        };
    }

    /**
     * @param {jQuery} $element
     */
    addElement($element)
    {
        this._addPropertyElement($element);
        this._addScrollToElement($element);
    }

    _addScrollToElement($element)
    {
        let href = '';

        if (!$element.is('a[href]') || (href = $element.attr('href')) === undefined || !href.startsWith(
                '#') || $element.attr('data-scroll-duration') === undefined) {
            return;
        }

        new ScrollToElement($element);
    }

    _addPropertyElement($element)
    {
        let dataScrollAnimation = $element.attr('data-scroll-animation');
        let dataScrollProperty = $element.attr('data-scroll-property');
        let dataScrollPropertyFrom = $element.attr('data-scroll-property-from');
        let dataScrollPropertyTo = $element.attr('data-scroll-property-to');
        let dataScrollFrom = $element.attr('data-scroll-from');
        let dataScrollTo = $element.attr('data-scroll-to');

        if ([dataScrollFrom, dataScrollTo].indexOf(undefined) >= 0) {
            return;
        }

        if (!([dataScrollProperty, dataScrollPropertyFrom, dataScrollPropertyTo].indexOf(undefined) >= 0 ||
                dataScrollAnimation === undefined)) {
            return;
        }

        this.property.registerElement($element);
    }
}

let animateScroll = new AnimateScroll();

# AnimateScroll

This script will (hopefully) helps you to create scroll based animations in HTML

## Demo
You can find a demo together with a HowTo [here](https://www.yannik-sc.de/AnimateScroll)

## How does it work technically?
The script itself uses a pre defined CSS animation (`@keyframes ...`) or creates one from given HTML attributes.
The animation will then be controlled from the JavaScript by a negative delay *done*.

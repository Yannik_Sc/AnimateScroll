#!/usr/bin/env bash

#
# This script requires
# - inotify-tools (linux only)
# - node-sass (available via npm: sudo npm i -g node-sass)
#

CSS_PATH=demo/css
CURRENT=$(pwd);

function compile {
    cd ${CSS_PATH};

    printf "Compiling ... "
    node-sass --output-style compressed --source-map true style.scss > style.min.css && echo Done

    cd ${CURRENT}
}

compile;

inotifywait -r -m -e modify ${CSS_PATH} --exclude style.min.css |
    while read path action file; do
        compile;
    done;
